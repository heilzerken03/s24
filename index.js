const getCube = 2**3
// console.log(getCube)

console.log(`The cube of 2 is: ${getCube}`)

// Address:
const full_address = ["B5 L5", "Maya Country Homes,","Dulumbayan,","Teresa Rizal,","1880",]
const [house_number, street_name, brgy_name, city_name, post_code] = full_address
console.log(`I live at ${house_number} ${street_name} ${brgy_name} ${city_name} ${post_code}`)

// animal:
const animal = {
	animal_type: "saltwater crocodile",
	animal_weight: "1075 kgs",
	animal_height: "20 ft 3 in."
}
const {animal_type, animal_weight, animal_height} = animal
console.log(`Lolong was a ${animal_type}. He weighed at ${animal_weight} with a measurement of ${animal_height}`)

// numbers:
const array_num = [1,2,3,4,5]
array_num.forEach((array_num) => {
	console.log(`${array_num}`)
	return `${array_num}`
})

let iteration = 0;
let reduceArray = array_num.reduce(function(x,y){
	return	x + y;
})

const reduceNumber = () => console.log(reduceArray)
reduceNumber();

// // dog:
// // const dog = {
// // 	dog_name: "Frankie",
// // 	dog_age: 5,
// // 	dog_breed: "Miniature Dachshund"
// // }

// class Dog {
// 	details(dog_name, dog_age, dog_breed){
// 		this.dog_name = dog_name;
// 		this.dog_age = dog_age;
// 		this.dog_breed = dog_breed;
// 	}
// }
// let dog = new Dog("Frankie", "5", "Miniature Dachshund");

// const {dog_name, dog_age, dog_breed} = dog
// console.log(`Dog ${dog_name} ${dog_age} ${dog_breed}`)


